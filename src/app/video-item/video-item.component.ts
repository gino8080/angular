import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { VideoModel } from '../models/video-model';

@Component({
  selector: 'app-magic-video-item',
  templateUrl: './video-item.component.html',
  styleUrls: ['./video-item.component.css']
})
export class VideoItemComponent implements OnInit {
  @Input() video: VideoModel;
  @Output() select = new EventEmitter();

  myBG: string = "white";
  isSelected: boolean;

  constructor() { }

  ngOnInit() { }

  onClicked(event: MouseEvent, clickedVideo: VideoModel) {
    event.stopPropagation();
    this.isSelected = !this.isSelected;
    console.log("clickedVideo!", clickedVideo.title, this.isSelected);

    this.select.emit({ video: clickedVideo })
  }

  onClickedList() {
    console.log("onClickedList")

    /* if (this.isSelected) {
       this.myBG = "green";
     } else {
       this.myBG = "white";
     }*/
  }
}
