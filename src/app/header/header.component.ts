import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-magic-header',
  //template : '<div></div>',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  title = 'Youtube Clone';
  searchTxt: string = ""; //valore di ricerca nel campo di input
  @Output() search = new EventEmitter(); //evento quando cerco

  getTitle = () => {
    return this.title;
  }

  onKeyEnter(event: any) {
    const inputEl: HTMLInputElement = event.target;
    const txt = inputEl.value;
    console.log("Premuto ENTER diretto!", txt);
    this.doSearch();
  }

  doSearch() {
    console.log("DO SEARCH", this.searchTxt)
    this.search.emit({ searchTerm: this.searchTxt })
  }
}
