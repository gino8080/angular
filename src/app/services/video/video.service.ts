import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { VideoModel } from '../../models/video-model';

const API_KEY = 'AIzaSyDsCNEiE11jyCqp7q6-K5uiyD-ji9AVfzU';


@Injectable({
  providedIn: 'root'
})
export class VideoService {
  constructor(private http: HttpClient) {

    this.callYoutube("dogs");
  }


  callYoutube(q: string = '') {

    const url = "https://www.googleapis.com/youtube/v3/search";
    const params = {
      part: "snippet",
      key: API_KEY,
      maxResults: "10",
      videoEmbeddable: "true",
      type: "video",
      q: q
    };

    return this.http.get(url, { params: params }).toPromise()

  }

}
