import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { VideoModel } from '../models/video-model';

@Component({
  selector: 'app-magic-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.css']
})
export class VideoListComponent implements OnInit {
  @Output() select = new EventEmitter();
  @Input() videoList: VideoModel[];

  ngOnInit() { }

  onSelect(event: any) {
    console.log("Ricevuto evento da Lista figlio", event)
    this.select.emit(event.video)
  }
}
