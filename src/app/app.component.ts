import { Component, OnInit } from '@angular/core';
import { VideoModel } from './models/video-model';
import { VideoService } from './services/video/video.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(private service: VideoService) { }

  ngOnInit() {
    this.searchVideos();
  }

  videos: VideoModel[] = [];
  currentVideo: VideoModel = null;
  currentIndex: number = 0;

  searchVideos(searchTerm: string = "") {
    this.service.callYoutube(searchTerm)
      .then((result: any) => {
        //tutto ok, ecco il risultato
        // console.log(result)
        const items: any[] = result.items;

        const videoItems: VideoModel[] = items.map(function (item, key) {
          // console.log(key, item);
          //ciclo il risultato di youtube e lo mappo con oggetti del mio tipo  VideoModel;
          const videoMod: VideoModel = {
            id: item.id.videoId,
            title: item.snippet.title,
            description: item.snippet.description,
            date: item.snippet.publishedAt,
            img: item.snippet.thumbnails.default.url,
            images: item.snippet.thumbnails
          }
          // console.log("videoMod", key, videoMod);
          return videoMod;
        })

        this.videos = videoItems;

        if (this.videos.length > 0) {
          this.currentVideo = this.videos[this.currentIndex]
        }

      })
      .catch(err => {
        //qualche errore
        console.error(err);
      })


    console.log("OK ")

  }

  onSearchTerm(event) {
    console.log("onSearchTerm", event)
    const searchTerm = event.searchTerm;
    this.searchVideos(searchTerm);
  }

  onSelectedVideo(video: VideoModel) {
    console.log("Ricevuto da Nonno", video.title, video);
    this.currentVideo = video;
  }

  prevVideo() {
    if (this.currentIndex > 0) {
      this.currentIndex--;
      this.currentVideo = this.videos[this.currentIndex]
    }
  }

  nextVideo() {
    if (this.currentIndex < this.videos.length) {
      this.currentIndex++;
      this.currentVideo = this.videos[this.currentIndex]
    }
  }
}
