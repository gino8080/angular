import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'readmore'
})
export class ReadmorePipe implements PipeTransform {

  transform(text: string, limit: number = 20): any {
    return text.substr(0, limit) + " ... LEGGI TUTTO";
  }

}
