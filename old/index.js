var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
// var nome: string = 'dsadsa';
// var age: number = 37;
var active = true;
var something = 'dsadsa';
something = 123;
var powers = ['invisiblity', 'poison'];
var powers2 = ['invisiblity', 'poison'];
var someList = ['dsadas', 123, true];
var addresses = ['Via le mani dal naso', 113];
function sum(x, y) {
    return x + y;
}
var sum2 = function (x, y) {
    return x + y;
};
var sum3 = function (x, y) { return x + y; }; //return one line instruction
var multiply = function (x) {
    return x * 2;
};
var multiply2 = function (x) {
    if (x === void 0) { x = 10; }
    return x * 2;
};
sum(10, 5);
var lastName = 'Pippo';
lastName = 'Gino';
var KEY = 'eqwd7e376321hu312';
//KEY = 'edasdsa';
var Gino = {
    nome: 'pippo',
    age: 37,
    speak: function (words) {
        return words;
    }
};
var ginoAge = Gino.age;
var nome = Gino.nome, age = Gino.age, speak = Gino.speak;
console.log('gino name è:', nome, age, 'ciao', 8908098908);
var lista = ['qui', 'quo', 'qua'];
var papero = lista[0], paper2 = lista[1], paper3 = lista[2];
console.log(papero);
var logga = function (txt) {
    var args = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        args[_i - 1] = arguments[_i];
    }
    console.log('LOGGA', txt, args);
};
logga('CIAO', 321, 'CIAOOOOO', true, Gino);
var Ginetto = __assign({}, Gino);
console.log('GINETTO', Ginetto);
//# sourceMappingURL=index.js.map