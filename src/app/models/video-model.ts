interface VideoModel {
  id: string;
  title: string;
  description: string;
  img: string;
  date: string;
  images: any[];
}

interface Pippo {
  id: number;
  title: string;
  img: string;
}
export { VideoModel, Pippo };
