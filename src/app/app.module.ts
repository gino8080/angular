import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";

import { LOCALE_ID, NgModule } from '@angular/core';
import { HttpClientModule } from "@angular/common/http";
import { registerLocaleData } from "@angular/common";
import itLang from "@angular/common/locales/it";

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { VideoListComponent } from './video-list/video-list.component';
import { MainVideoComponent } from './main-video/main-video.component';
import { VideoService } from './services/video/video.service';
import { VideoItemComponent } from './video-item/video-item.component';
import { SafePipe } from './safe.pipe';
import { ReadmorePipe } from './readmore.pipe';
import { ContactsComponent } from './contacts/contacts.component';
import { HomeComponent } from './home/home.component';

registerLocaleData(itLang, 'it');


const routes = [
  {
    path: "contact",
    component: ContactsComponent
  },

  {
    path: "*",
    component: HomeComponent
  }
]
@NgModule({
  // tutti i componenti utilizzati nella mia applicazione
  declarations: [
    AppComponent,
    HeaderComponent,
    VideoListComponent,
    MainVideoComponent,
    VideoItemComponent,
    SafePipe,
    ReadmorePipe,
    ContactsComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    // RouterModule.forRoot(routes)
  ],
  // servizi
  providers: [
    VideoService,
    { provide: LOCALE_ID, useValue: 'it' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
