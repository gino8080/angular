// var nome: string = 'dsadsa';
// var age: number = 37;
var active: boolean = true;
var something: any = 'dsadsa';
something = 123;

var powers: string[] = ['invisiblity', 'poison'];
var powers2: Array<string> = ['invisiblity', 'poison'];
var someList: any[] = ['dsadas', 123, true];

var addresses: [string, number] = ['Via le mani dal naso', 113];

//Custom type
type Person = {
  nome: string;
  age: number;
  speak: (txt: string) => void;
};

function sum(x: number, y: number) {
  return x + y;
}
var sum2 = function(x: number, y: number) {
  return x + y;
};

var sum3 = (x: number, y: number) => x + y; //return one line instruction

var multiply = (x: number) => {
  return x * 2;
};
var multiply2 = (x: number = 10) => x * 2;

sum(10, 5);

let lastName: string = 'Pippo';
lastName = 'Gino';

const KEY = 'eqwd7e376321hu312';
//KEY = 'edasdsa';

var Gino: Person = {
  nome: 'pippo',
  age: 37,
  speak: function(words: string) {
    return words;
  }
};

let ginoAge = Gino.age;

let { nome, age, speak } = Gino;

console.log('gino name è:', nome, age, 'ciao', 8908098908);

let lista: string[] = ['qui', 'quo', 'qua'];

let [papero, paper2, paper3] = lista;

console.log(papero);

const logga = (txt: string, ...args: any[]) => {
  console.log('LOGGA', txt, args);
};

logga('CIAO', 321, 'CIAOOOOO', true, Gino);

const Ginetto = { ...Gino };

console.log('GINETTO', Ginetto);
