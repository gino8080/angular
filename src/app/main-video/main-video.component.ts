import { Component, OnInit, Input } from '@angular/core';
import { VideoModel } from '../models/video-model';

@Component({
  selector: 'app-magic-main-video',
  templateUrl: './main-video.component.html',
  styleUrls: ['./main-video.component.css']
})
export class MainVideoComponent implements OnInit {
  @Input() currentVideo: VideoModel = null;
  path: string = 'https://www.youtube.com/embed/';
  constructor() { }

  ngOnInit() {
  }

}
